#TripleD; A 3D Library for Love2D
TripleD is a library for Love2D that simulates 3D on a 2D engine.

##Changelog
For the changelog, see [Changelog.md](https://bitbucket.org/substitute541/tripled/src/2ad0d6ce732f/Changelog.md?at=master).
--[[ Triangle3D class ]]--

local Triangle3D = {}
Triangle3D.p1 = Point3D:new()
Triangle3D.p2 = Point3D:new()
Triangle3D.p3 = Point3D:new()

function Triangle3D:new()
	local o = {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Triangle3D:getNormal(isUnitVector)
	isUnitVector = isUnitVector or true
	local ab = {
		x = self.p1.x - self.p2.x,
		y = self.p1.y - self.p2.y,
		z = self.p1.z - self.p2.z
	}
	local bc = {
		x = self.p2.x - self.p3.x,
		y = self.p2.y - self.p3.y,
		z = self.p2.z - self.p3.z
	}
	local norm = {
		x = (ab.y * bc.z) - (ab.z - bc.y),
		y = -((ab.x * bc.z) - (ab.z - bc.x)),
		z = (ab.x * bc.y) - (ab.y * bc.x)
	}
	if isUnitVector then
		local mag = sqrt(norm.x^2 + norm.y^2 + norm.z^2)
		norm.x = norm.x/mag
		norm.y = norm.y/mag
		norm.z = norm.z/mag
	end
	return norm.x, norm.y, norm.z
end

return Triangle3D
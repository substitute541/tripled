#Changelog

##0.4;
* Added the Triangle3D module.
* Added more options for creating a Point3D object.

##0.3:
* Changed TD to be a global variable, due to some issues with requiring files
* Moved the part for the Point3D class into the `class` folder.
* Added TD.rotate().
* Renamed all of the `rx/ry/rz` (rotation variables) to `ax/ay/az`.

##0.2:
* Added camera rotation support.

##0.1:
* Inital Release.
--[[----------------------------------------||
  || TripleD 3D Library by substitute541
  || Version: 0.3
  ||
  ||    Copyright (C) 2013  Phoenix Enero
  ||
  || This program is free software: you can redistribute it and/or modify
  || it under the terms of the GNU General Public License as published by
  || the Free Software Foundation, either version 3 of the License, or
  || (at your option) any later version.
  ||
  || This program is distributed in the hope that it will be useful,
  || but WITHOUT ANY WARRANTY; without even the implied warranty of
  || MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  || GNU General Public License for more details.
--]]----------------------------------------||

TD = {}
TD.f = 250
TD.vpx = love.graphics.getWidth()/2
TD.vpy = love.graphics.getHeight()/2

--[ Modules ]--
TD.camera = {
	ax = 0,
	ay = 0,
	az = 0
}
TD.class = {}

--[ Require all needed files ]--
TD.class.Point3D = require "class.Point3D"

--[[ Functions ]]--

-- Sets the default values
function TD.setVals(f, vpx, vpy)
	TD.f = f
	TD.vpx = vpx
	TD.vpy = vpy
end

-- Rotates camera
function TD.rotateCamera(ax, ay, az)
	TD.camera.ax = TD.camera.ax + ax
	TD.camera.ay = TD.camera.ay + ay
	TD.camera.az = TD.camera.az + az
end

-- Sets camera rotation
function TD.setCameraRotation(ax, ay, az)
	TD.camera.ax = ax or 0
	TD.camera.ay = ay or 0
	TD.camera.az = az or 0
end

-- Calculate 2D coordinates based on 3D coordinates
function TD.calculate(x, y, z, doNotInvert)
	doNotInvert = doNotInvert or false
	
	-- Rotate coordinates depending on camera angle
	local cam = TD.camera
	x1, y1, z1 = TD.rotate(x, y, z, cam.ax, cam.ay, cam.az, 0, 0, -TD.f)
	
	-- If the user does not want the inverting effect...
	if doNotInvert then
		-- ... and it's apparently inverting...
		if TD.f + z1 <= 0 then
			-- ...return huge values and a scale of zero
			return math.huge, math.huge, 0
		end
	end
	
	-- Translating all of that into 2D...
	local scale = TD.f/(TD.f + z1)
	
	return x1*scale + TD.vpx, y1*scale + TD.vpy, scale
end

-- Rotates 2D coordinates around a center point, or around the origin
function TD.rotate(x, y, z, ax, ay, az, cx, cy, cz)
	cx = cx or 0
	cy = cy or 0
	cz = cz or 0
	
	-- X Rotation
	local cosX = math.cos(ax)
	local sinX = math.sin(ax)
	local ty = (y-cy)
	local y1 = ty * cosX - (z-cz) * sinX
	local z1 = (z-cz) * cosX + ty * sinX
	
	-- Y Rotation
	local cosY = math.cos(ay)
	local sinY = math.sin(ay)
	local tx = (x-cx)
	local x1 = tx * cosY - (z1-cz) * sinY
	local z1 = (z1-cz) * cosY + tx * sinY
	
	-- Z Rotation
	local cosZ = math.cos(az)
	local sinZ = math.sin(az)
	local tx = (x1-cx)
	local x1 = tx * cosZ - (y1-cy) * sinZ
	local y1 = (y1-cy) * cosZ + tx * sinZ
	
	return x1, y1, z1
end
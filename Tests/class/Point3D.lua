--[[ Point3D class ]]--

local Point3D = {}
Point3D.pos = {
	x = 0,
	y = 0,
	z = 0
}
Point3D.x = 0
Point3D.y = 0
Point3D.scale = 1
Point3D.doNotInvert = true

function Point3D:new()
	local o = {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Point3D:update()
	self.x, self.y, self.scale = TD.calculate(self.pos.x, self.pos.y, self.pos.z, self.doNotinvert)
end

return Point3D
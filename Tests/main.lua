-- Test 2 for TripleD

require "TripleD"

function love.load()
	point = TD.class.Point3D:new()
	point.pos.x = -50
	point.pos.y = -50
end

local timer = 0
function love.update(dt)
	timer = timer + dt
	point:update()
	--point.pos.z = math.sin(timer * 5) * 5
	TD.rotateCamera(3 * dt, 4*dt, 1 * dt)
end

function love.draw()
	love.graphics.circle("fill", point.x, point.y, point.scale * 50)
end